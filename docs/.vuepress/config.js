module.exports = {
    title: 'TechnoTrumpet Blog v2 Via Vuepress',
    description: 'TechnoTrumpets Blog served using VuePress',
    base: '//vuepress/',
    dest: 'public'
}